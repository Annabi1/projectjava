package com.jwt.code.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jwt.code.model.Formation;
@Repository
public interface FormationDao extends JpaRepository<Formation,String> {

}
