package com.jwt.code.dao;
import com.jwt.code.model.Groupe;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupeDao  extends JpaRepository<Groupe,Long>{

}
