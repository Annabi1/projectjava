package com.jwt.code.dao;
import com.jwt.code.model.Qcm;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QcmDao extends JpaRepository<Qcm,Long> {

}
