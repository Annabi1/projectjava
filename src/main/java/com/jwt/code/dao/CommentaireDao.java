package com.jwt.code.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import com.jwt.code.model.Commentaire;
public interface CommentaireDao extends JpaRepository<Commentaire,Long> {

}
