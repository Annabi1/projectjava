package com.jwt.code.controller;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jwt.code.model.Commentaire;
import com.jwt.code.service.CommentaireServiceImp;
@RestController
@RequestMapping("/formations")
@CrossOrigin(origins = "*", maxAge = 3600)
public class CommentaireRestController {
	@Autowired
	CommentaireServiceImp  c;

	@PostMapping("/CreateCommentaire")
	public Commentaire CreateCommentaire(@RequestBody Commentaire com) {
		// TODO Auto-generated method stub
		return c.CreateCommentaire(com);
	}

	@PutMapping("/UpdateCommentaire")
	public Commentaire UpdateCommentaire(@RequestBody Commentaire com) {
		// TODO Auto-generated method stub
		return c.UpdateCommentaire(com);
	}

	@DeleteMapping("/deleteCommentaire/{id}")
	public void deleteCommentaire(@PathVariable Long id) {
		// TODO Auto-generated method stub
		c.deleteCommentaire(id);
	}

	@GetMapping("/GetAllCommentaire")
	public List<Commentaire> GetAllCommentaire() {
		// TODO Auto-generated method stub
		return c.GetAllCommentaire();
	}

	@GetMapping("/GetCommentaireByName/{id}")
	public Optional<Commentaire> GetCommentaireByName(@PathVariable Long id) {
		// TODO Auto-generated method stub
		return c.GetCommentaireByName(id);
	}

}
