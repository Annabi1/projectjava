package com.jwt.code.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.code.dao.QcmDao;
import com.jwt.code.model.Qcm;

@RestController
@RequestMapping("/formations")
@CrossOrigin(origins = "*", maxAge = 3600)

public class QcmRestController {
	
	@Autowired
	QcmDao q;
	@PostMapping("/CreateQcm")
	public Qcm CreateQcm( @RequestBody Qcm qcm) {
		// TODO Auto-generated method stub
		return q.save(qcm);
	}

	@PutMapping("/UpdateQcm")
	public Qcm UpdateQcm(  @RequestBody Qcm qcm) {
		// TODO Auto-generated method stub
		return  q.save(qcm);
	}

	@DeleteMapping("/deleteQcm/{id}")
	public void deleteQcm( @PathVariable Long id) {
		// TODO Auto-generated method stub
		q.deleteById(id);
		
	}

	@GetMapping("/GetAllQcm")
	public List<Qcm> GetAllQcm() {
		// TODO Auto-generated method stub
		return q.findAll();
	}

	@GetMapping("/GetQcmById/{id}")
	public Optional<Qcm> GetQcmById(@PathVariable Long id) {
		// TODO Auto-generated method stub
		return q.findById(id);
	}
	
	

}
