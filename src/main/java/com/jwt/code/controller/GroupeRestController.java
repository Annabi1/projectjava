package com.jwt.code.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.code.dao.GroupeDao;
import com.jwt.code.model.Groupe;

@RestController
@RequestMapping("/formations")
@CrossOrigin(origins = "*", maxAge = 3600)
public class GroupeRestController {
	
	@Autowired
	GroupeDao g;


	@PostMapping("/CreateGroupe")
	public Groupe CreateGroupe(@RequestBody Groupe groupe) {
		// TODO Auto-generated method stub
		return g.save(groupe);
	}

	@PutMapping("/UpdateGroupe")
	public Groupe UpdateGroupe(@RequestBody Groupe groupe) {
		// TODO Auto-generated method stub
		return g.save(groupe);
	}

	@DeleteMapping("/deleteGroupe/{id}")
	public void deleteGroupe(@PathVariable  Long id) {
		// TODO Auto-generated method stub
		g.deleteById(id);
	}

	@GetMapping("/GetAllGroupe")
	public List<Groupe> GetAllGroupe() {
		// TODO Auto-generated method stub
		return g.findAll();
	}

	@GetMapping("/GetGroupeByName/{id}")
	public Optional<Groupe> GetGroupeByName(@PathVariable  Long id) {
		// TODO Auto-generated method stub
		return g.findById(id);
	}

}
