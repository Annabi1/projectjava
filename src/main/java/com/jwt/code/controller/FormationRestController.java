package com.jwt.code.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.code.dao.FormationDao;
import com.jwt.code.model.Formation;
@RequestMapping("/formations")
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class FormationRestController {
	
	@Autowired
	FormationDao f;
	
	@PostMapping("/CreateFormation")
	public Formation CreateFormation(@RequestBody Formation formation) {
		// TODO Auto-generated method stub
		return f.save(formation);
	}

	@PutMapping("/UpdateFormation")
	public Formation UpdateFormation( @RequestBody Formation formation) {
		// TODO Auto-generated method stub
		return f.save(formation);
	}

	@DeleteMapping("/deleteFormation/{name}")
	public void deleteFormation(@PathVariable String name) {
		// TODO Auto-generated method stub
		f.deleteById(name);
	}

	@GetMapping("/GetAllFormation")
	public List<Formation> GetAllFormation() {
		// TODO Auto-generated method stub
		return f.findAll();
	}

	@GetMapping("/GetFormationByName/{name}")
	public Optional<Formation> GetFormationByName(@PathVariable String name ) {
		// TODO Auto-generated method stub
		return f.findById(name);
	}


}
