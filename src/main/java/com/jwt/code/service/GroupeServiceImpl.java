package com.jwt.code.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.code.dao.GroupeDao;
import com.jwt.code.model.Groupe;

@Service
public class GroupeServiceImpl implements GroupeService {
	@Autowired
	GroupeDao g;

	@Override
	public Groupe CreateGroupe(Groupe groupe) {
		// TODO Auto-generated method stub
		return g.save(groupe);
	}

	@Override
	public Groupe UpdateGroupe(Groupe groupe) {
		// TODO Auto-generated method stub
		return g.save(groupe);
	}

	@Override
	public void deleteGroupe(Long id) {
		// TODO Auto-generated method stub
		g.deleteById(id);
	}

	@Override
	public List<Groupe> GetAllGroupe() {
		// TODO Auto-generated method stub
		return g.findAll();
	}

	@Override
	public Optional<Groupe> GetGroupeByName(Long id) {
		// TODO Auto-generated method stub
		return g.findById(id);
	}



}
