package com.jwt.code.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.jwt.code.model.Groupe;

@Service
public interface GroupeService {
	Groupe CreateGroupe(Groupe groupe);
	Groupe UpdateGroupe(Groupe groupe);
	void deleteGroupe(Long id);
	List <Groupe> GetAllGroupe();
	Optional<Groupe> GetGroupeByName(Long id);
	
}
