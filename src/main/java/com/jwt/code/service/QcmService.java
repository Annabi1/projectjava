package com.jwt.code.service;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import com.jwt.code.model.Qcm;
@Service
public interface QcmService {
	Qcm CreateQcm(Qcm qcm);
	Qcm UpdateQcm(Qcm qcm);
	void deleteQcm(Long id);
	List <Qcm> GetAllQcm();
	Optional<Qcm> GetQcmById(Long id );
	
	

}