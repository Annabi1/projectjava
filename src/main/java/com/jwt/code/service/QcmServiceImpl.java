package com.jwt.code.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.code.dao.QcmDao;
import com.jwt.code.model.Qcm;
@Service
public class QcmServiceImpl implements QcmService {
	@Autowired
	QcmDao q;

	@Override
	public Qcm CreateQcm(Qcm qcm) {
		// TODO Auto-generated method stub
		return q.save(qcm);
	}

	@Override
	public Qcm UpdateQcm(Qcm qcm) {
		// TODO Auto-generated method stub
		return  q.save(qcm);
	}

	@Override
	public void deleteQcm(Long id) {
		// TODO Auto-generated method stub
		q.deleteById(id);
		
	}

	@Override
	public List<Qcm> GetAllQcm() {
		// TODO Auto-generated method stub
		return q.findAll();
	}

	@Override
	public Optional<Qcm> GetQcmById(Long id) {
		// TODO Auto-generated method stub
		return q.findById(id);
	}

}
