package com.jwt.code.service;

import java.util.List;
import java.util.Optional;
import com.jwt.code.dao.FormationDao;
import org.springframework.beans.factory.annotation.Autowired;

import com.jwt.code.model.Formation;

public class FormationServiceImpl implements FormationService {
	@Autowired
	FormationDao f;

	@Override
	public Formation CreateFormation(Formation formation) {
		// TODO Auto-generated method stub
		return f.save(formation);
	}

	@Override
	public Formation UpdateFormation(Formation formation) {
		// TODO Auto-generated method stub
		return f.save(formation);
	}

	@Override
	public void deleteFormation(String name) {
		// TODO Auto-generated method stub
		f.deleteById(name);
	}

	@Override
	public List<Formation> GetAllFormation() {
		// TODO Auto-generated method stub
		return f.findAll();
	}

	@Override
	public Optional<Formation> GetFormationByName(String name ) {
		// TODO Auto-generated method stub
		return f.findById(name);
	}


}
