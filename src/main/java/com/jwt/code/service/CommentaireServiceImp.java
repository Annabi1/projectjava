package com.jwt.code.service;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.code.dao.CommentaireDao;
import com.jwt.code.model.Commentaire;
@Service
public class CommentaireServiceImp  implements CommentaireService{
	@Autowired
	CommentaireDao c;

	@Override
	public Commentaire CreateCommentaire(Commentaire com) {
		// TODO Auto-generated method stub
		return c.save(com);
	}
	@Override
	public Optional<Commentaire> GetCommentaireByName(Long id) {
		// TODO Auto-generated method stub
		return c.findById(id);
	}

	@Override
	public Commentaire UpdateCommentaire(Commentaire com) {
		Commentaire cm=c.findById(com.getId_commentaire()).get();
		cm.setDescription(com.getDescription());

		// TODO Auto-generated method stub
		return c.save(com);
	}

	@Override
	public void deleteCommentaire(Long id) {
		// TODO Auto-generated method stub
		c.deleteById(id);
	}

	@Override
	public List<Commentaire> GetAllCommentaire() {
		// TODO Auto-generated method stub
		return c.findAll();
	}

	

}
