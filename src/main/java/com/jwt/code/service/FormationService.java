package com.jwt.code.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.jwt.code.model.Formation;

@Service
public interface FormationService {
	Formation CreateFormation(Formation formation);
	Formation UpdateFormation(Formation formation);
	void deleteFormation(String name);
	List <Formation> GetAllFormation();
	Optional<Formation> GetFormationByName(String name);
	

}
