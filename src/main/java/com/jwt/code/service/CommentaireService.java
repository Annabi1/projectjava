package com.jwt.code.service;
import java.util.List;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.jwt.code.model.Commentaire;

@Service
public interface CommentaireService {
	Commentaire CreateCommentaire(Commentaire com);
	Commentaire UpdateCommentaire(Commentaire com);
	void deleteCommentaire(Long id);
	List <Commentaire> GetAllCommentaire();
	Optional<Commentaire> GetCommentaireByName(Long id);

}
