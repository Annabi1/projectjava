package com.jwt.code.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
@Entity
@Table(name="formation")
public class Formation {
	@Id
	@Column(name="nom")
	private String nom;
	@Column(name="date_de_debut")
	private String date_de_debut;
	@Column(name="date_de_fin")
	private String date_de_fin;
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the date_de_debut
	 */
	public String getDate_de_debut() {
		return date_de_debut;
	}
	/**
	 * @param date_de_debut the date_de_debut to set
	 */
	public void setDate_de_debut(String date_de_debut) {
		this.date_de_debut = date_de_debut;
	}
	/**
	 * @return the date_de_fin
	 */
	public String getDate_de_fin() {
		return date_de_fin;
	}
	/**
	 * @param date_de_fin the date_de_fin to set
	 */
	public void setDate_de_fin(String date_de_fin) {
		this.date_de_fin = date_de_fin;
	}
	
	

}
