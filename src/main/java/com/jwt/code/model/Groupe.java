package com.jwt.code.model;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name="Groupe")
public class Groupe {
	
		@Id 
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Long id_groupe ;
		@Column(name="nom")
		private String nom;
		/**
		 * @return the nom
		 */
		public String getNom() {
			return nom;
		}
		/**
		 * @param nom the nom to set
		 */
		public void setNom(String nom) {
			this.nom = nom;
		}
		


}
