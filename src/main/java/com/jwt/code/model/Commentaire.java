package com.jwt.code.model;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name="Commentaire")
public class Commentaire {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_commentaire ;
	@Column(name="description")
	private String description;
	/**
	 * @return the description
	 */
	
	public String getDescription() {
		return description;
	}
	/**
	 * @return the id_commentaire
	 */
	
	/**
	 * @param id_commentaire the id_commentaire to set
	 */
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId_commentaire() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
