package com.jwt.code.model;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name="Qcm")
public class Qcm {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_qcm ;
	@Column(name="question")
	private String question;
	@Column(name="reponse1")
	private String reponse1;
	@Column(name="reponse2")
	private String reponse2;
	@Column(name="reponse3")
	private String reponse3;
	@Column(name="reponsevraie")
	private String reponsevraie;
	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	/**
	 * @return the reponse
	 */
	public String getReponse() {
		return reponse1;
	}
	/**
	 * @param reponse the reponse to set
	 */
	public void setReponse(String reponse) {
		this.reponse1 = reponse;
	}
	/**
	 * @return the reponse2
	 */
	public String getReponse2() {
		return reponse2;
	}
	/**
	 * @param reponse2 the reponse2 to set
	 */
	public void setReponse2(String reponse2) {
		this.reponse2 = reponse2;
	}
	/**
	 * @return the reponse3
	 */
	public String getReponse3() {
		return reponse3;
	}
	/**
	 * @param reponse3 the reponse3 to set
	 */
	public void setReponse3(String reponse3) {
		this.reponse3 = reponse3;
	}
	/**
	 * @return the reponsevraie
	 */
	public String getReponsevraie() {
		return reponsevraie;
	}
	/**
	 * @param reponsevraie the reponsevraie to set
	 */
	public void setReponsevraie(String reponsevraie) {
		this.reponsevraie = reponsevraie;
	}
	
	
	
	
}

