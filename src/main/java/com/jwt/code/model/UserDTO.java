package com.jwt.code.model;

public class UserDTO {
	private long cin;
	private String username;
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the cin
	 */
	public long getCin() {
		return cin;
	}

	/**
	 * @param cin the cin to set
	 */
	public void setCin(long cin) {
		this.cin = cin;
	}
	
}
