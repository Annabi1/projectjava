package com.jwt.code.model;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "user")
@Inheritance(strategy = InheritanceType.JOINED)
public class DAOUser {
	@Id
	private long cin;
	@Column
	private String username;
	@Column
	@JsonIgnore
	private String password;
	
	

	/**
	 * @return the cin
	 */
	public long getCin() {
		return cin;
	}
	/**
	 * @param cin the cin to set
	 */
	public void setCin(long cin) {
		this.cin = cin;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
